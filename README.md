## Init venv

```bash
poetry env use python
```

## Use env

```bash
poetry shell
```

## Run app

```bash
poetry run 
```