import uvicorn

def dev():
    uvicorn.run("app.main:app", port=5000, reload=True)